/*
 * A simple programme that will change the intensity of
 * an LED based  * on the amount of light incident on 
 * the photo resistor.
 * 
 */

//PhotoResistor Pin
int lightPin = 0; //the analog pin the photoresistor is 
                  //connected to
                  //the photoresistor is not calibrated to any units so
                  //this is simply a raw sensor value (relative light)
//LED Pin
int doorOpenPin = 8; //the pin the LED is connected to
                  //we are controlling brightness so 
                  //we use one of the PWM (pulse width
                  // modulation pins)
int doorClosePin = 9;

boolean stateDoorClosed = true;
int motorRunningTime = 0;

void setup()
{
  pinMode(doorOpenPin, OUTPUT); //sets the led pin to output
  pinMode(doorClosePin, OUTPUT);
  Serial.begin(9600);
}
 /*
 * loop() - this function will start after setup 
 * finishes and then repeat
 */
void loop(){
  int threshold = 400;
  char buffer[20];
  int lightPinValue = analogRead(lightPin);
  sprintf(buffer, "lightPin=%d", lightPinValue);
  Serial.println(buffer);
  if(lightPinValue > threshold and !stateDoorClosed){
    digitalWrite(doorClosePin, HIGH);
    sprintf(buffer, "closing door");
    Serial.println(buffer);
    delay(41000);
    stateDoorClosed = true;
    digitalWrite(doorClosePin, LOW);
  }
  else if(lightPinValue < threshold and stateDoorClosed){
    digitalWrite(doorOpenPin, HIGH);
    sprintf(buffer, "opening door");
    Serial.println(buffer);
    delay(40000);
    stateDoorClosed = false;
    digitalWrite(doorOpenPin, LOW);
  }
  delay(5000);
}
